use std::{
    fmt::{self, Debug, Formatter},
    io,
    num::ParseIntError,
    str::{self, Utf8Error},
};

/// Error raised when the parser encounters an error.
///
#[derive(Debug, Fail, PartialEq)]
pub enum ParseError {
    #[fail(display = "parse utf8 error: {}", _0)]
    Utf8Error(Utf8Error),

    #[fail(display = "parse datetime error: {}", _0)]
    ParseDateTimeError(chrono::format::ParseError),

    #[fail(display = "parse int error: {}", _0)]
    ParseIntError(ParseIntError),

    #[fail(display = "parse error caused by: {:?}", _0)]
    ParseError(nom::error::ErrorKind),
}

impl From<Utf8Error> for ParseError {
    #[inline]
    fn from(error: Utf8Error) -> ParseError {
        ParseError::Utf8Error(error)
    }
}

impl From<chrono::format::ParseError> for ParseError {
    #[inline]
    fn from(error: chrono::format::ParseError) -> ParseError {
        ParseError::ParseDateTimeError(error)
    }
}

impl From<ParseIntError> for ParseError {
    #[inline]
    fn from(error: ParseIntError) -> ParseError {
        ParseError::ParseIntError(error)
    }
}

impl<'a> From<nom::error::ErrorKind> for ParseError {
    #[inline]
    fn from(error: nom::error::ErrorKind) -> ParseError {
        ParseError::ParseError(error)
    }
}

/// Error that includes the input that the parser failed on.
///
#[derive(PartialEq)]
pub(crate) struct ParseErrorWithInput<'a>(pub &'a [u8], pub ParseError);

impl<'a> nom::error::ParseError<&'a [u8]> for ParseErrorWithInput<'a> {
    #[inline]
    fn from_error_kind(input: &'a [u8], kind: nom::error::ErrorKind) -> Self {
        ParseErrorWithInput(input, kind.into())
    }

    #[inline]
    fn append(_: &[u8], _: nom::error::ErrorKind, other: Self) -> Self {
        other
    }
}

impl<'a> Debug for ParseErrorWithInput<'a> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        let (input, error) = (&self.0, &self.1);

        match str::from_utf8(input) {
            Ok(s) => write!(fmt, "ParseErrorWithInput({:?}, {:?})", s, error),
            Err(_) => write!(fmt, "ParseErrorWithInput({:?}, {:?})", input, error),
        }
    }
}

/// Error raised when reading a stream or parsing.
///
#[derive(Debug, Fail)]
pub enum ReadParseError {
    #[fail(display = "read error: {}", _0)]
    ReadError(io::Error),

    #[fail(display = "parse error: {}", _0)]
    ParseError(ParseError),
}

impl From<io::Error> for ReadParseError {
    #[inline]
    fn from(error: io::Error) -> ReadParseError {
        ReadParseError::ReadError(error)
    }
}

impl From<ParseError> for ReadParseError {
    #[inline]
    fn from(error: ParseError) -> ReadParseError {
        ReadParseError::ParseError(error)
    }
}
