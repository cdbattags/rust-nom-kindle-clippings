pub mod borrowed;
pub mod owned;

#[derive(Debug, Eq, PartialEq)]
pub struct Page(pub u32);

#[derive(Debug, Eq, PartialEq)]
pub struct LocationRange(pub u32, pub u32);
