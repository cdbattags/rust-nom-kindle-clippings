use crate::clipping::{LocationRange, Page};
use chrono::prelude::{DateTime, Utc};

#[derive(Debug, Eq, PartialEq)]
pub struct Bookmark {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: Option<LocationRange>,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Highlight {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: String,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Note {
    pub title: String,
    pub author: Option<String>,
    pub page: Option<Page>,
    pub location: LocationRange,
    pub created_at: DateTime<Utc>,
    pub content: String,
}

#[derive(Debug, Eq, PartialEq)]
pub enum Clipping {
    Bookmark(Bookmark),
    Highlight(Highlight),
    Note(Note),
}
